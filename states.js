var states = [
        {
        'name':'discard',
        'initial': true,
        'events':{
            'pile':'pile',
            'deck':'deck'
        } 
    },
    {
        'name':'pile',
        'events':{
            'knock':'knock',
            'discard':'discard'
        }
        
    },
    {
        'name':'deck',
        'events':{
            'knock':'knock',
            'discard':'discard'
        }
    },
    {
        'name':'knock',
        'events':{
            'pile':'pile',
            'deck':'deck',
            'submitted':'submitted'
        }   
    },
    {
        'name': 'submitted',
        'events':{
            'gameover': 'gameover'
        }
    },
    {
        'name': 'gameover',
        'events':{
            'discard': 'discard'
        },
        //After game is over calculate the score
        action : function(player, opponenet){
            var score;

            //Take in both player and opponent
            if (player.deadwood === 0){
                player.score += 25 + opponenet.deadwood;
                score = player.score;
            }
            else if (player.deadwood >= opponenet.deadwood){
                opponenet.score += 25 + player.deadwood;
                score = opponenet.score;
            }
            else{
                player.score += opponenet.deadwood - player.deadwood;
                score =  player.score;
            }
            //Check for Gin vs Knock
            //Calculate score
            //Add value to score
            return score;
        }
    }
];

var StateMachine = function () { 
    this.currentState = '';
    this.indexes = {};
    for (var i= 0; i<states.length; i++){
    	this.indexes[states[i].name] = i;
        if (states[i].initial){
            this.currentState = states[i];
        }
    }
    //console.log(currentState);
    //return currentState;
};

StateMachine.prototype.nextState = function(next){
	//console.log(currentState.events);
	if (this.currentState.events[next]){
		this.currentState = states[this.indexes[this.currentState.events[next]]];
	}
};

StateMachine.prototype.getState = function(){
	return this.currentState;
}

exports.init = StateMachine;
