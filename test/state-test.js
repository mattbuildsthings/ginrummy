var state = require('../states.js');

var sm = new state.init();

exports['discard'] = function (test) {
	test.equal(sm.getState().name, 'discard');
	test.done();
}

exports['pile'] =  function (test){
	sm.nextState('pile');
	test.equal(sm.getState().name, 'pile');
	test.done();
}

exports['knock'] = function(test){
	sm.nextState('knock');
	test.equal(sm.getState().name, 'knock');
	test.done();
}

exports['submitted'] = function(test){
	sm.nextState('submitted');
	test.equal(sm.getState().name, 'submitted');
	test.done();
}

exports['gameover'] = function(test){
	sm.nextState('gameover');
	test.equal(sm.getState().name, 'gameover');

	var player1 = {
	  'name': 'player1',
	  'hand': '',
	  'score': 0,
	  'deadwood': 0,
	}
	var player2 = {
	  'name': 'player2',
	  'hand': '',
	  'score': 0,
	  'deadwood': 0,
	}
	test.equal(sm.getState().action(player1, player2), 25);

	player1.deadwood = 5;
	test.equal(sm.getState().action(player1, player2), 30);

	player1.deadwood = 5;
	player2.deadwood = 25;
	test.equal(sm.getState().action(player1, player2), 45);
	test.equal(player2.score, 30);

	test.done();
}
