var state = require('./states.js');

//Function creates deck, shuffles, deals to both players
var Game = function(room){
    this.player1 = {'hand':[], 'deadwood' : 0, 'score': 0};
    this.player2 = {'hand':[], 'deadwood' : 0, 'score': 0};
    this.room = room;
    console.log(room);
}

Game.prototype.newGame =  function(){
    this.cardDeck = [];
    this.discard = [];
    this.player1.hand = [];
    this.player2.hand = [];
    this.cardCount = 21;
    this.turn = '';
    this.sm = new state.init()

    for( var i=0; i<52; i++){
        if (i<13){
         suit = 'hearts';
         suit_val = 3;
        }
        if (i>12 && i<26)
        {
         suit = 'clubs';
         suit_val = 0;
        }
        if (i>25 && i<39)
        {
         suit = 'diamonds';
         suit_val = 2;
        }
        if (i>38)
        {
         suit = 'spades';
         suit_val = 1;
        }
        this.cardDeck.push({
            value: (i%13)+1,
            suit: suit,
            suit_val: suit_val,
            group: 0
        });
    }

    //For loop performs a shuffle on the Cards
    for(var i=0; i<52; i++){
        var randNum=Math.floor(Math.random()*51);
        var tmp = this.cardDeck[i];
        this.cardDeck[i] = this.cardDeck[randNum];
        this.cardDeck[randNum] = tmp;
    }

    for(var i=0; i<20; i++){
        if ((i%2)==0){
            this.player1.hand.push(this.cardDeck[i]);
        }
        else{
            this.player2.hand.push(this.cardDeck[i]);
        }
    }

    this.discard.push(this.cardDeck[20]);
};

//Sort the players hand by suit and then value
Game.prototype.sortHand = function(player){
    player.sort(function (x, y){
        if (x.suit_val == y.suit_val){
            return x.value - y.value;
        }
        else
            return x.suit_val - y.suit_val;
    });
};

//Pick up a card from the Deck
Game.prototype.drawDeck = function(player){
    player.push(this.cardDeck[this.cardCount]);
    this.cardCount++;
};

//Pick up a card from the discard pile
Game.prototype.drawDisc = function(player){
    player.push(this.discard[this.discard.length-1]);
    this.discard.splice(this.discard.length-1, 1);
}

//Drop a card from the player into the discard pile
Game.prototype.dropCard = function(player, suit, value){
    for (var i=0; i<player.length; i++){
        if (player[i].suit == suit && player[i].value == value){
            this.discard.push(player[i]);
            player.splice(i, 1);
            return true;
        }
    }
    return false;
};

var cardCompare = function(section, card, nextCard){
    if (section.type == ''){
        section.anchor = card;
        if (nextCard === undefined){
            section.valid = false;
        }
        else if (nextCard.value == (card.value+1) && nextCard.suit_val == 
           card.suit_val && nextCard.group == card.group){
            section.type = 'straight';
        }
        else{
            section.type = 'pairs';
        }
    }
    else if (section.type == 'straight' && section.valid === true){
        if (card.value == (section.anchor.value +1) && card.suit_val ==
            section.anchor.suit_val){
            section.anchor =  card;
        }
        else{
            section.valid = false;
        }
    }
    else if (section.type == 'pairs' && section.valid === true){
        if (card.value == section.anchor.value){
            section.achor = card;
        }
        else{
            section.valid = false;
        }
    }
    if (card.value >= 10){
        section.deadwood += 10
    }
    else {
        section.deadwood += card.value;
    }
    if (section.max_val < card.value){
        section.max_val = card.value;
    }
    section.count +=1;
};

var checkDeadwood = function(section){
    if (section.valid ===false || section.count < 3){
        return section.deadwood;
    }
    else{
        return 0;
    }
};

var checkMaxVal = function(section, max_value){
    if (section.max_val > max_value && (section.valid === false ||
        section.count < 3)){
        max_value = section.max_val;
    }
    return max_value;
};


//Function that is used to check if each section submited when knocked is valid
//If the section is not full, the deadwood is added to the total value
Game.prototype.checkGin = function(player, hand, state){
    var section0 = {anchor:'', type:'', deadwood:0, valid:true, count:0, max_val:0};
    var section1 = {anchor:'', type:'', deadwood:0, valid:true, count:0, max_val:0};
    var section2 = {anchor:'', type:'', deadwood:0, valid:true, count:0, max_val:0};
    var section3 = {anchor:'', type:'', deadwood:0, valid:true, count:0, max_val:0};
    var deadwood = 0;
    var max_value = 0;

    for (var i=0; i<hand.length; i++){
        if (hand[i].value == player[i].value && hand[i].suit == 
            player[i].suit){
            if (hand[i].group == 0){
                cardCompare(section0, hand[i], hand[i+1]);
            }

            if (hand[i].group == 1){
                cardCompare(section1, hand[i], hand[i+1]);
            }

            if (hand[i].group == 2){
                cardCompare(section2, hand[i], hand[i+1]);
            }

            if (hand[i].group == 3){
                cardCompare(section3, hand[i], hand[i+1]);
            }
        }
    }
    
    deadwood += checkDeadwood(section0);
    deadwood += checkDeadwood(section1);
    deadwood += checkDeadwood(section2);
    deadwood += checkDeadwood(section3);

    max_value = checkMaxVal(section0, max_value);
    max_value = checkMaxVal(section1, max_value);
    max_value = checkMaxVal(section2, max_value);
    max_value = checkMaxVal(section3, max_value);

    //Removes highest card from deadwood for knocker
    if (state == 'knock'){
        if (max_value >= 10){
            max_value = 10;
        }
        deadwood = deadwood - max_value;
    }

    //console.log(section0);
    //console.log(section1);
    //console.log(section2);
    //console.log(section3);

    return deadwood;
};

exports.init = Game;

