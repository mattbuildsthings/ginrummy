var express = require('express')
  ,	app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , game = require('./game.js');

io.set('log level', 1); // reduce logging

app.use("/styles", express.static(__dirname + '/styles'));
app.use("/js", express.static(__dirname + '/js'));
app.use("/images", express.static(__dirname + '/images'));

server.listen(80);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

var count = 0;

var games = [];
var rooms = [];
var users = [];

//game.sortHand(new_game.player1);
//game.drawDeck(new_game.player1, new_game);
//game.sortHand(new_game.player1);
//console.log(new_game.player1);

var StartGame = function(player1, player2, room){
  var ng = new game.init(room);
  ng.newGame();

  games.push(ng);

  //player1.hand = ng.player1.hand;
  //player2.hand = ng.player2.hand;
  ng.player1.id = player1;
  ng.player2.id = player2;

  ng.sortHand(ng.player1.hand);
  ng.sortHand(ng.player2.hand);

  ng.turn = ng.player1.id;

  io.sockets.socket(ng.player1.id).emit('get_hand', ng.player1.hand);
  io.sockets.socket(ng.player1.id).emit('turn', 'Your Turn');
  io.sockets.socket(ng.player2.id).emit('get_hand', ng.player2.hand);
  io.sockets.socket(ng.player2.id).emit('turn', 'Opponents Turn');
  io.sockets.socket(ng.player1.id).emit('scoreboard', "Your Score: " + ng.player1.score
                                     + " - Opponents Score: "+ ng.player2.score);
  io.sockets.socket(ng.player2.id).emit('scoreboard', "Your Score: " + ng.player2.score
                                     + " - Opponents Score: "+ ng.player1.score);
  io.sockets.in(ng.room).emit('discard_card', ng.discard[ng.discard.length-1]);

  io.sockets.in(ng.room).emit('clear_opponent');
}

var NewHand = function(ng){
  ng.newGame();

  ng.sortHand(ng.player1.hand);
  ng.sortHand(ng.player2.hand);

  ng.turn = ng.player1.id;

  io.sockets.socket(ng.player1.id).emit('get_hand', ng.player1.hand);
  io.sockets.socket(ng.player1.id).emit('turn', 'Your Turn');
  io.sockets.socket(ng.player2.id).emit('get_hand', ng.player2.hand);
  io.sockets.socket(ng.player2.id).emit('turn', 'Opponents Turn');
  io.sockets.socket(ng.player1.id).emit('scoreboard', "Your Score: " + ng.player1.score
                                     + " - Opponents Score: "+ ng.player2.score);
  io.sockets.socket(ng.player2.id).emit('scoreboard', "Your Score: " + ng.player2.score
                                     + " - Opponents Score: "+ ng.player1.score);
  io.sockets.in(ng.room).emit('discard_card', ng.discard[ng.discard.length-1]);

  io.sockets.in(ng.room).emit('clear_opponent');
}

io.sockets.on('connection', function (socket) {

  //Displays all rooms currently available on cennection
  socket.emit('update_rooms', rooms);

  //Creates a room and sends back list of rooms. 
  //Checks to make sure no repeated names or multiple rooms per user
  socket.on('create_room', function(data){
    for (i=0; i<rooms.length; i++){
      if (rooms[i]===data){
        io.sockets.socket(socket.id).emit('error', 'Room Name Already Exists');
        return false;
      }
    }
    for (i=0; i<users.length; i++){
      if (users[i].id === socket.id){
        io.sockets.socket(socket.id).emit('error', 'You are already in a Room');
        return false;
      }
    }
    user = {'id':socket.id, 'room': data};
    users.push(user);
    room = data;
    socket.join(room);
    rooms.push(room);
    io.sockets.emit('update_rooms', rooms);
  });

  //Join an existing room and start a game
  socket.on('join_room', function(room){
    var p2;
    var user;
    var players = io.sockets.clients(room);
    if (players.length > 1){
      return false;
    }
    for (i=0; i<rooms.length; i++){
      if (rooms[i] === room){
        user = {'id':socket.id, 'room': room};
        p2 = user;
        socket.join(room);
      }
    }
    for (i=0; i<users.length; i++){
      if (users[i].room === room && users[i].id !== socket.id){
        p1 = users[i];
      }
      if (users[i].id === socket.id){
        io.sockets.socket(socket.id).emit('error', 'You are already in a Room');
        return false;
      }
    }
    users.push(user);
    StartGame(p1.id, p2.id, user.room);
  });

  //Function for when the client discards a card
  socket.on('discard', function(selected_card){
    for (j=0; j<games.length; j++){
      if (socket.id == games[j].turn && games[j].sm.currentState.events['discard']){
        var player = {}
        if (socket.id === games[j].player1.id){
          player = games[j].player1;
          opponent = games[j].player2;
        }
        if (socket.id === games[j].player2.id){
          player = games[j].player2;
          opponent = games[j].player1;
        }
        var inHand = games[j].dropCard(player.hand, selected_card.suit, selected_card.value);
        if(inHand === true){
      	  io.sockets.socket(player.id).emit('get_hand', player.hand);
          io.sockets.in(games[j].room).emit('discard_card', games[j].discard[games[j].discard.length-1]);
      	  if (socket.id == games[j].player1.id){
            games[j].turn = games[j].player2.id;
          }
          if (socket.id == games[j].player2.id){
            games[j].turn = games[j].player1.id;
          }
          io.sockets.socket(opponent.id).emit('turn', 'Your Turn');
          io.sockets.socket(player.id).emit('turn', 'Opponents Turn');
          games[j].sm.nextState('discard');
        }
      }
    }
  });

  //Function for when the client tries to draw from the pile
  socket.on('pile', function(){
    for (j= 0; j<games.length; j++ ){
      if (socket.id === games[j].turn && games[j].sm.currentState.events['pile']){
        var player = {}
        if (socket.id === games[j].player1.id){
          player = games[j].player1;
        }
        if (socket.id === games[j].player2.id){
          player = games[j].player2;
        }
        games[j].drawDisc(player.hand);
        games[j].sortHand(player.hand);
        io.sockets.socket(player.id).emit('get_hand', player.hand);
        io.sockets.in(games[j].room).emit('discard_card', games[j].discard[games[j].discard.length-1]);
        games[j].sm.nextState('pile');
      }
    }
  });

  //Function for when the client tries to draw from the deck
  socket.on('deck', function(){
    for(j=0; j<games.length; j++){
      if (socket.id == games[j].turn && games[j].sm.currentState.events['deck']){
        var player = {}
        if (socket.id == games[j].player1.id){
          player = games[j].player1;
        }
        if (socket.id == games[j].player2.id){
          player = games[j].player2;
        }
        games[j].drawDeck(player.hand);
        games[j].sortHand(player.hand);
        io.sockets.socket(player.id).emit('get_hand', player.hand);
        io.sockets.in(games[j].room).emit('discard_card', games[j].discard[games[j].discard.length-1]);
        games[j].sm.nextState('deck');
      }
    }
  });

  //Function for when the client tries to knock or gin
  socket.on('knock', function(data){
    //Steps for the player who knocked
    for(j=0; j<games.length; j++){
      if (socket.id == games[j].turn && games[j].sm.currentState.events['knock']){
        var player = {};
        var opponent = {};
        var deadwood;
        if (socket.id == games[j].player1.id){
          player = games[j].player1;
          opponent = games[j].player2;
        }
        if (socket.id == games[j].player2.id){
          player = games[j].player2;
          opponent =  games[j].player1;
        }
        
        deadwood = games[j].checkGin(player.hand, data, 'knock');

        if (deadwood <=10){
          games[j].sm.nextState('knock');
          player.deadwood = deadwood;
          games[j].turn = opponent.id;
          games[j].sm.nextState('submitted');
          io.sockets.socket(player.id).emit('knock_submit', opponent.hand);
          io.sockets.socket(opponent.id).emit('knock_submit', player.hand);
        }
        else{
          games[j].sm.nextState('pile');
        }
        console.log(deadwood);
      }

      //Steps for the opponent 
      if (socket.id == games[j].turn && games[j].sm.currentState.events['gameover']){
        var player = {};
        var deadwood;
        if (socket.id == games[j].player1.id){
          opponent = games[j].player1;
          player = games[j].player2;
        }
        if (socket.id == games[j].player2.id){
          opponent = games[j].player2;
          player =  games[j].player1;
        }

        console.log(data);

        deadwood = games[j].checkGin(opponent.hand, data, 'submitted');

        opponent.deadwood = deadwood;

        games[j].sm.nextState('gameover');
        games[j].sm.getState().action(player, opponent);

        console.log(player);
        console.log(opponent);

        NewHand(games[j]);
      }
    }
  });

});