var canvas = document.getElementById('p1_canvas');
var context = canvas.getContext('2d');
var imageObj = new Image();
imageObj.src= '/images/cards.gif';
var width =73;
var height =97;
var card = 10;
var player = [];
var selected_card= {};

function getMousePos(canvas, e){
	var rect = canvas.getBoundingClientRect();
	return{
		x: e.clientX - rect.left,
		y: e.clientY - rect.top 
	};
 }

function drawSelectedCard (e){
	var mousePos = getMousePos(canvas, e);

	for(var i=0; i<player.length; i++){
		context.drawImage(imageObj, width*player[i].value, height*player[i].suit_val, width, height, 
						  40*player[i].value, 97*player[i].suit_val, width, height);
		if (mousePos.x >= player[i].value*40 && mousePos.x <= player[i].value*40 + width &&
			mousePos.y >= player[i].suit_val*97 && mousePos.y <= player[i].suit_val*97 + height){

			if (selected_card == player[i-1]){
				context.drawImage(imageObj, width*player[i-1].value, height*player[i-1].suit_val, width, height, 
						  	  	  40*player[i-1].value, 97*player[i-1].suit_val, width, height);
				context.drawImage(imageObj, width*player[i].value, height*player[i].suit_val, width, height, 
						  		  40*player[i].value, 97*player[i].suit_val, width, height);
			}

			if (i != player.length-1){
				if ((player[i+1].value-player[i].value)==1 && player[i+1].suit_val==player[i].suit_val)
					var slice_width=40;
				else
					var slice_width=73;
			}
			else
				var slice_width=73;


			var imageData = context.getImageData(40*player[i].value, 97*player[i].suit_val, slice_width, height);
			var data = imageData.data;

			for(var j = 0; j < data.length; j += 4) {
    		    data[j + 2] = 0;
    		}
    		
    		selected_card = player[i];

    		context.putImageData(imageData, 40*player[i].value, 97*player[i].suit_val);
		}
	}
}

function drawDiscard (e) {
	context.drawImage(imageObj, 0, height, width, height, 650, 100, width, height);
}