function Game() {
	this.player1 = [];
	this.player2 = [];
	var cardDeck = [];
	var discard = [];
	var cardCount = 20;

    //Function creates deck, shuffles, deals to both players
    this.deal = function(){
        for( var i=0; i<52; i++){
            if (i<13){
             suit = 'hearts';
             suit_val = 3;
            }
            if (i>12 && i<26)
            {
             suit = 'clubs';
             suit_val = 0;
            }
            if (i>25 && i<39)
            {
             suit = 'diamonds';
             suit_val = 2;
            }
            if (i>38)
            {
             suit = 'spades';
             suit_val = 1;
            }
            cardDeck.push({
                value: (i%13)+1,
                suit: suit,
                suit_val: suit_val
            });
        }
    
        for(var i=0; i<52; i++){
            var randNum=Math.floor(Math.random()*51);
            var tmp = cardDeck[i];
            cardDeck[i] = cardDeck[randNum];
            cardDeck[randNum] = tmp;
        }
    
        for(var i=0; i<20; i++){
            if ((i%2)==0){
                this.player1.push(cardDeck[i]);
            }
            else{
                this.player2.push(cardDeck[i]);
            }
        }
    };

    this.sortHand = function(player){
        player.sort(function (x, y){
            if (x.suit_val == y.suit_val){
                return x.value - y.value;
            }
            else
                return x.suit_val - y.suit_val;
        });
    };

    //Pick up a card from the Deck
    this.drawDeck = function(player){
        player.push(cardDeck[cardCount]);
        cardCount++;   
    };

    //Pick up a card from the discard pile
    this.drawDisc = function(player){
        player.push(discard[discard.length-1]);
        discard.splice(discard.length-1, 1);
    }

    //Drop a card from the player into the discard pile
    this.dropCard = function(player, suit, value){
        for (var i=0; i<player.length; i++){
            if (player[i].suit == suit && player[i].value == value){
                discard.push(player[i]);
                player.splice(i, 1);
            }
        }
    };

    this.printDisc = function(){
        $('#discard_pile').html('');
        for (var i = 0; i<discard.length ; i++) {
            $('#discard_pile').append(discard[i].suit + discard[i].value +'\n');
        }
    };
}

Game.prototype.print = function(player, location){
    $(location).html('');
	for (var i=0; i<player.length; i++){
		$(location).append(player[i].suit + player[i].value +'\n');
	}
};

