function addToGin (e){
    var mousePos = getMousePos(canvas, e);
    var selected_card;
    var ginDeck;

    for(var i=0; i<player.length; i++){

        if (mousePos.x >= player[i].value*40 && mousePos.x <= player[i].value*40 + width &&
            mousePos.y >= player[i].suit_val*97 && mousePos.y <= player[i].suit_val*97 + height){

                selected_card = i;

                if (player[i] != player[player.length-1]){
                    if (player[i+1].value == (player[i].value+1) && 
                        player[i+1].suit_val == player[i].suit_val){
                        width = 40;
                    }
                    else{
                        width = 73;
                    }
                }
                else{
                    width =73;
                }
        }
    }

    if (selected_card !== undefined){
        player[selected_card].group = player[selected_card].group + 1;

        context.drawImage(imageObj, 73*player[selected_card].value, 
                          height*player[selected_card].suit_val, width, height, 
                          40*player[selected_card].value, 
                          97*player[selected_card].suit_val, width, height);

        if (player[selected_card].group == 1){
            context.fillStyle = "rgba(0, 0, 200, 0.5)";
            context.fillRect(40*player[selected_card].value, 
                             97*player[selected_card].suit_val,width,height);
        }
        if (player[selected_card].group == 2){
            context.fillStyle = "rgba(0, 200, 0, 0.5)";
            context.fillRect(40*player[selected_card].value, 
                             97*player[selected_card].suit_val,width,height);
        }
        if (player[selected_card].group == 3){
            context.fillStyle = "rgba(200, 0, 0, 0.5)";
            context.fillRect(40*player[selected_card].value, 
                             97*player[selected_card].suit_val,width,height);
        }
        if (player[selected_card].group == 4){
            player[selected_card].group = 0;
        }
    }

    width = 73;
}