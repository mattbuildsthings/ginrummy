socket.on('update_rooms', function(rooms){
    $('#room_list').html('');
    for (i=0; i< rooms.length; i++){
        $('#room_list').append('<div class="room" value="'
            +rooms[i]+'">' + rooms[i] + '</div>');
    }
});

socket.on('room_message', function(data){
    console.log(data);
});

$("#button").click(function(){
    var room = $("#room_name").val();
    socket.emit('create_room', room);
    return false;
});

$("#message").click(function(){
    socket.emit('message_room');
    return false;
});

$('#room_list').on('click', '.room', function(){
    var room = $(this).html();
    socket.emit('join_room', room);
});